package com.commit451.gitlab.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.commit451.addendum.design.snackbar
import com.commit451.addendum.themeAttrColor
import com.commit451.gitlab.App
import com.commit451.gitlab.R
import com.commit451.gitlab.api.converter.DashDateAdapter
import com.commit451.gitlab.databinding.ActivityAddMilestoneBinding
import com.commit451.gitlab.event.MilestoneChangedEvent
import com.commit451.gitlab.event.MilestoneCreatedEvent
import com.commit451.gitlab.extension.checkValid
import com.commit451.gitlab.extension.with
import com.commit451.gitlab.model.api.Milestone
import com.commit451.teleprinter.Teleprinter
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import io.reactivex.rxjava3.core.Single
import timber.log.Timber
import java.util.*

class AddMilestoneActivity : MorphActivity() {

    companion object {

        private const val KEY_PROJECT_ID = "project_id"
        private const val KEY_MILESTONE = "milestone"

        @JvmOverloads
        fun newIntent(context: Context, projectId: Long, milestone: Milestone? = null): Intent {
            val intent = Intent(context, AddMilestoneActivity::class.java)
            intent.putExtra(KEY_PROJECT_ID, projectId)
            if (milestone != null) {
                intent.putExtra(KEY_MILESTONE, milestone)
            }
            return intent
        }
    }

    private lateinit var binding: ActivityAddMilestoneBinding
    private val fullscreenProgress by lazy {
        binding.root.findViewById<View>(R.id.fullscreenProgress)
    }
    lateinit var teleprinter: Teleprinter

    var projectId: Long = 0
    var milestone: Milestone? = null
    var currentDate: Date? = null

    private val onDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            currentDate = calendar.time
            bind(calendar.time)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddMilestoneBinding.inflate(layoutInflater)
        setContentView(binding.root)
        morph(binding.root)
        teleprinter = Teleprinter(this)
        projectId = intent.getLongExtra(KEY_PROJECT_ID, -1)
        milestone = intent.getParcelableExtra(KEY_MILESTONE)
        if (milestone != null) {
            bind(milestone!!)
            binding.toolbar.inflateMenu(R.menu.edit)
        } else {
            binding.toolbar.inflateMenu(R.menu.create)
        }
        binding.toolbar.setNavigationIcon(R.drawable.ic_back_24dp)
        binding.toolbar.setNavigationOnClickListener { onBackPressed() }
        binding.toolbar.setOnMenuItemClickListener(Toolbar.OnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.action_create, R.id.action_edit -> {
                    createMilestone()
                    return@OnMenuItemClickListener true
                }
            }
            false
        })
        binding.buttonDueDate.setOnClickListener {
            val now = Calendar.getInstance()
            currentDate?.let {
                now.time = it
            }
            val dpd = DatePickerDialog.newInstance(
                onDateSetListener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
            )
            dpd.accentColor = this.themeAttrColor(R.attr.colorAccent)
            dpd.show(supportFragmentManager, "date_picker")
        }
    }

    private fun createMilestone() {
        teleprinter.hideKeyboard()
        if (!binding.textInputLayoutTitle.checkValid()) {
            return
        }

        fullscreenProgress.visibility = View.VISIBLE
        var dueDate: String? = null
        val currentDate = currentDate
        if (currentDate != null) {
            dueDate = DashDateAdapter.format.format(currentDate)
        }

        if (milestone == null) {
            createOrEditMilestone(
                App.get().gitLab.createMilestone(
                    projectId,
                    binding.textTitle.text.toString(),
                    binding.textDescription.text.toString(),
                    dueDate
                )
            )
        } else {
            createOrEditMilestone(
                App.get().gitLab.editMilestone(
                    projectId,
                    milestone!!.id,
                    binding.textTitle.text.toString(),
                    binding.textDescription.text.toString(),
                    dueDate
                )
            )
        }

    }

    private fun createOrEditMilestone(observable: Single<Milestone>) {
        observable.with(this)
            .subscribe({
                fullscreenProgress.visibility = View.GONE
                if (milestone == null) {
                    App.bus().post(MilestoneCreatedEvent(it))
                } else {
                    App.bus().post(MilestoneChangedEvent(it))
                }
                finish()
            }, {
                Timber.e(it)
                fullscreenProgress.visibility = View.GONE
                showError()
            })
    }

    private fun showError() {
        binding.root.snackbar(R.string.failed_to_create_milestone)
    }

    fun bind(date: Date) {
        binding.buttonDueDate.text = DashDateAdapter.format.format(date)
    }

    fun bind(milestone: Milestone) {
        binding.textTitle.setText(milestone.title)
        if (milestone.description != null) {
            binding.textDescription.setText(milestone.description)
        }
        if (milestone.dueDate != null) {
            currentDate = milestone.dueDate
            bind(currentDate!!)
        }
    }
}
